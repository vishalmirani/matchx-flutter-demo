import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:flutter/material.dart';
import '../main.dart' as app;

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  final binding = IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  if (binding is LiveTestWidgetsFlutterBinding) {
    binding.framePolicy = LiveTestWidgetsFlutterBindingFramePolicy.fullyLive;
  }

  group('end-to-end test', () {
    testWidgets('Verify Match X Test Case Validation.',

      (tester) async {
              app.main();

              await tester.pumpAndSettle();

              // Assuming there is register form with First Name, Last Name, Phone, Country & Submit Button

              // With below action First Name will be insert in field

              await tester.enterText(find.byKey(const ValueKey('FirstNameField')), 'Vishal');

              // With below action Last Name will be insert in field

              await tester.enterText(find.byKey(const ValueKey('LastNameField')), 'Mirani');

              // With below action Phone will be insert in field

              await tester.enterText(find.byKey(const ValueKey('PhoneField')), '+917016025052');

              // Now I will skip Country field to enter value and direct click on submit button

              await tester.tap(find.byType(SubmitActionButton));

              // Now I will verify the Error message text appear on screen for mandatory field

              // Note: Here Our First Test will be verify as per requirement.

              expect(find.text('Please enter your current residing Country'), findsOneWidget);

              // Now with below action, We will insert Country Name in field

              await tester.enterText(find.byType(const ValueKey('CountryField')), 'Germany');

              // Now I am again click on Submit button to save the register details

              await tester.tap(find.byType(SubmitActionButton));

              await addDelay(1000);

              // Now I will verify the Response message text appear on screen after click on Submit button.

              // Note: Here Our Second Test will be verify as per requirement.

              expect(find.text('Data is in processing.'), findsOneWidget);

              tester.printToConsole('Register Data submit successfully');

              await tester.pumpAndSettle();

      });
  });
}